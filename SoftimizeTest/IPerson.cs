﻿using System;

namespace SoftimizeTest
{
    public interface IPerson
    {
        int GetId();
        string GetFirstName();
        string GetLastName();
        DateTime GetDateofBirth();
        int GetHeight();
    }
}
