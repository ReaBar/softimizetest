﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;

namespace SoftimizeTest
{
    public class PersonCollection : INotifyCollectionChanged, ICustomCollection<IPerson>
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private readonly IComparer<IPerson> _comparer;
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        private readonly LinkedList<IPerson> _personsLinkedList = new LinkedList<IPerson>();

        public PersonCollection(IComparer<IPerson> comparer)
        {
            _comparer = comparer;
        }

        public void Add(IPerson obj)
        {
            _lock.EnterWriteLock();
            try
            {
                _personsLinkedList.AddFirst(obj);
                SortLinkedList(_comparer);
                OnCollectionChanged(NotifyCollectionChangedAction.Add);
            }
            finally
            {
                if (_lock.IsWriteLockHeld) _lock.ExitWriteLock();
            }
        }

        public IPerson Remove()
        {
            _lock.EnterWriteLock();
            try
            {
                if (_personsLinkedList.Count > 0)
                {
                    IPerson tempPerson = _personsLinkedList.Last.Value;
                    _personsLinkedList.RemoveLast();
                    OnCollectionChanged(NotifyCollectionChangedAction.Remove);
                    return tempPerson;
                }
                return null;
            }
            finally
            {
                if (_lock.IsWriteLockHeld) _lock.ExitWriteLock();
            }
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action)
        {
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(action, _personsLinkedList));
        }

        private void SortLinkedList(IComparer<IPerson> comparer)
        {
            LinkedListNode<IPerson> personX = _personsLinkedList.First;
            LinkedListNode<IPerson> personY = personX.Next;

            while (personY != null)
            {
                if (comparer.Compare(personX.Value, personY.Value) > 0)
                {
                    LinkedListNode<IPerson> tempNode = new LinkedListNode<IPerson>(personX.Value);
                    _personsLinkedList.Remove(personX);
                    personX = tempNode;
                    _personsLinkedList.AddAfter(personY, personX);
                    personY = personX.Next;
                }
                else
                {
                    break;
                }
            }
        }
    }
}
