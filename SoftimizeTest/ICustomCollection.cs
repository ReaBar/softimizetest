﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftimizeTest
{
    interface ICustomCollection<T>
    {
        void Add(T obj);
        T Remove();
    }
}
