﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftimizeTest;

namespace TestSoftimizeProject
{
    class Person : IPerson
    {
        private int _id, _height;
        private string _firstName, _lastName;
        private DateTime _birthDate;

        public Person(int id, int height, string fname, string lname, DateTime birthDate)
        {
            _id = id;
            _height = height;
            _firstName = fname;
            _lastName = lname;
            _birthDate = birthDate;
        }

        public int GetId()
        {
            return _id;
        }

        public string GetFirstName()
        {
            return _firstName;
        }

        public string GetLastName()
        {
            return _lastName;
        }

        public DateTime GetDateofBirth()
        {
            return _birthDate;
        }

        public int GetHeight()
        {
            return _height;
        }
    }
}
