﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftimizeTest;

namespace TestSoftimizeProject
{
    class Program
    {
        static void Main(string[] args)
        {
            IComparer<IPerson> compByHeight = new CompareByHeight();
            PersonCollection pc = new PersonCollection(compByHeight);
            IPerson p1 = new Person(1,183,"Rea","Bar",new DateTime(1989,06,13));
            IPerson p2 = new Person(12,164,"Michal","Yaakov",new DateTime(1989,06,01));
            IPerson p3 = new Person(123, 180,"Roy","Magen", new DateTime(1974,03,27));
            IPerson p4 = new Person(1234,190,"Amnon", "Bar", new DateTime(1939,02,17));

            //Console.WriteLine(pc.Remove()?.GetId());
            pc.Add(p1);
            pc.Add(p2);
            pc.Add(p4);
            Console.WriteLine(pc.Remove().GetId());
            Console.WriteLine(pc.Remove().GetId());
            pc.Add(p3);
            Console.WriteLine(pc.Remove()?.GetId());
            //Console.WriteLine(pc.Remove()?.GetId());
            //Console.WriteLine(pc.Remove()?.GetId());
            //Console.WriteLine(pc.Remove()?.GetId());
        }
    }
}
