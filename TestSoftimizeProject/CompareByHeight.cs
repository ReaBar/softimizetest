﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftimizeTest;

namespace TestSoftimizeProject
{
    class CompareByHeight : IComparer<IPerson>
    {
        public int Compare(IPerson x, IPerson y)
        {
            if (x == null && y == null || x.GetHeight() == y.GetHeight())
            {
                return 0;
            }

            if (y == null || x.GetHeight() > y.GetHeight())
            {
                return 1;
            }

            if (x == null || y.GetHeight() > x.GetHeight())
            {
                return -1;
            }

            return 0;
        }
    }
}
